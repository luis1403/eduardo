Compiladores:

Para compilar no Linux é necessario criar um arquivo, por exemplo "pico teste.py", depois deixamos o arquivo executavel com o "chmod a+x teste.py", e por fim executamos 
no terminal "python3 ./teste.py".

Para compilar no Windonx, é necessario abrir o arquivo sem está na IDE.

Executando o programa:

O programa faz cem transferencias, da conta1 para conta2 e viceversa a partir de cem threads. Para resolver o problema de concorrência foram usadas travas, 
provindas da biblioteca Threading, isso impede que as threads acessem um mesmo objeto ao mesmo tempo e evita o problema de concorrência.

foi usado a classe mythread para atribuir a trava e os valores das contas, posteriormente foi criado uma função dentro da classe que recebe o valor 10, referente ao valor 
de transferência. no começo da função, foram criadas as 100 threads a partir do for in range, então foi colocado uma trava que se solta no final da função, 
fazendo com que apenas uma thread funcione por vez, eliminando a concorrencia. 

Por fim, criamos um algoritmo randomico de 0 e 1. Toda a vez que uma thread é criada é atribuido um "0" ou "1" para a fazer randomicamente 
as trasferências e acrescentamos uma condição que impede que as contas se negativem.

Resultados:
conseguimos executar as cem threads fazendo transferencias em ambas as contas, sem haver travamento.
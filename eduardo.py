# -*- coding: utf-8 -*-
import threading
import random
import time


class mythread(threading.Thread):
    def __init__(self, trava, conta1, conta2):
        self.trava = trava
        self.conta1 = conta1
        self.conta2 = conta2

    def conta(self, valor):
            for i in range(0, 100):               # cria 100 threads
                trocar = random.randint(0, 1)           #sorteia números de 0 a 1 a cada nova thread
                print(trocar)             #mostra que tstá trocando entre 0 e 1
                trava_thread.acquire()           #trava
                if trocar == 1:             #se a variavel troca for == 1 entra na condição
                    if self.conta1 > 0:
                        self.conta1 = self.conta1 - valor       #passa da conta 1 para a conta 2
                        self.conta2 = self.conta2 + valor
                    else:
                        if self.conta2 > 0:
                            self.conta1 = self.conta1 + valor       #passa da conta 2 para a conta 1
                            self.conta2 = self.conta2 - valor
                else:       #se a variável troca for diferente de "1" ela entra no else
                    if self.conta2 > 0:
                        self.conta1 = self.conta1 + valor       # passa da conta 2 para a conta 1
                        self.conta2 = self.conta2 - valor
                    else:
                        self.conta1 = self.conta1 - valor       # passa da conta 1 para a conta 2
                        self.conta2 = self.conta2 + valor

                print('transferencia concluida com sucesso', i) # i == as threads criadas
                print('valor da conta 1', self.conta1)
                print('valor conta 2', self.conta2)
                trava_thread.release()                      #"solta" a trava para a próxima thread

trava_thread = threading.Lock()

if __name__ == '__main__':

    thread = mythread(trava_thread, 100, 100)       #cria a classe Thread com as contas 1 e 2.
    thread.conta(10) # chama a funçao run

print('\033[31msaindo da thread principal\033[31m')
time.sleep(1000)
